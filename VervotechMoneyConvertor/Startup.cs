using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using VervotechMoneyConvertor.Logger;
using VervotechMoneyConvertor.Models;
using VervotechMoneyConvertor.Services;
using ILogger = VervotechMoneyConvertor.Logger.ILogger;

namespace VervotechMoneyConvertor
{
    public class Startup
    {
    
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;

        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
             services.Configure<CurrencyDatabaseSettings>(
        Configuration.GetSection(nameof(CurrencyDatabaseSettings)));

        services.AddSingleton<ICurrencyDatabaseSettings>(sp =>
        sp.GetRequiredService<IOptions<CurrencyDatabaseSettings>>().Value);

            services.AddTransient<ILogger, FileLogger>();
            services.AddHttpClient();
            services.AddSingleton<CurrencyService>();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "VervotechMoneyConvertor", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "VervotechMoneyConvertor v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
