using System;
using System.Collections.Generic;

namespace VervotechMoneyConvertor.Logger
{
    public class In_Memory : ILogger
    {
        private Dictionary<long, Log> dataDict = new Dictionary<long, Log>();
        public void LogData(Log log)
        {
            var currentTicks = DateTime.Now.Ticks;
            dataDict.Add(currentTicks, log);
        }
    }
}
