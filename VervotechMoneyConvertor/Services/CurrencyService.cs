using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using VervotechMoneyConvertor.Models;
using System.Net.Http.Headers;
using System.IO;
using System.Net;
using System;
using MongoDB.Driver;

namespace VervotechMoneyConvertor.Services
{
    public class CurrencyService
    {
        
         private readonly IHttpClientFactory _clientFactory;

        public CurrencyService(IHttpClientFactory clientFactory)
        {
            this._clientFactory = clientFactory;
           
        }


        public CurrencyResponse currencyResponses { get; set; }

        public async Task<CurrencyResponse> GetCurrency(string FromCurrency)
        {
            string apiUrl = "https://api.ratesapi.io/api/latest?base=" + FromCurrency;
            var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);
            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStreamAsync();
                currencyResponses = await JsonSerializer.DeserializeAsync<CurrencyResponse>(responseStream);
            }
            else
            {
                currencyResponses = new CurrencyResponse();
            }
            return currencyResponses;
        }


        public async Task<List<ConvertedCurrency>> GetConvertedValue(List<Currency> exchange)
        {
            List<Task<CurrencyResponse>> data = new List<Task<CurrencyResponse>>();

            List<ConvertedCurrency> getList = new List<ConvertedCurrency>();

            foreach (Currency getAmount in exchange)
            {
                try
                {
                    if (getAmount.FromCurrency == String.Empty || getAmount.FromCurrency == null || getAmount.Amount.Equals(null))
                    {
                        throw new ArgumentException("Arguments cannot be null or empty");

                    }
                    Task<CurrencyResponse> task = GetCurrency(getAmount.FromCurrency);

                    data.Add(task);

                    if (data.Count == 3)
                    {
                        Task.WaitAll(data.ToArray());
                        getList.AddRange(await CallMax(exchange, data));
                        data.Clear();
                    }

                    if (data.Count > 0)
                    {
                        Task.WaitAll(data.ToArray());
                        getList.AddRange(await CallMax(exchange, data));
                        data.Clear();
                    }
                }
                catch (System.Exception)
                {
                    throw new ArgumentNullException("Arguments cannot be null or empty");
                }

            }
            return getList;
        }
        public async Task<List<ConvertedCurrency>> CallMax(List<Currency> exchange, List<Task<CurrencyResponse>> data)
        {
            List<ConvertedCurrency> result = new List<ConvertedCurrency>();

            foreach (Currency getdata in exchange)
            {
                string currency = getdata.ToCurrency;
                decimal value = 0;

                foreach (Task<CurrencyResponse> response in data)
                {


                    switch (getdata.ToCurrency)
                    {
                        case "PHP":
                            value = (await response).rates.PHP;
                            break;
                        case "USD":
                            value = (await response).rates.USD;
                            break;
                        case "GBP":
                            value = (await response).rates.GBP;
                            break;
                        case "HKD":
                            value = (await response).rates.HKD;
                            break;
                        case "IDR":
                            value = (await response).rates.IDR;
                            break;
                        case "ILS":
                            value = (await response).rates.ILS;
                            break;
                        default:
                            break;
                    }

                    ConvertedCurrency getMoney = new ConvertedCurrency();

                    getMoney.ConvertedAmount = getdata.Amount * value;
                    getMoney.Amount = getdata.Amount;
                    getMoney.ToCurrency = getdata.ToCurrency;
                    getMoney.FromCurrency = getdata.FromCurrency;

                    result.Add((getMoney));
                }
            }
            return result;
        }

    }
}
