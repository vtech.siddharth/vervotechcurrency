using System;

namespace VervotechMoneyConvertor.Models
{
    public class CurrencyDatabaseSettings : ICurrencyDatabaseSettings
    {
        public string CurrencyCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface ICurrencyDatabaseSettings
    {
        string CurrencyCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
