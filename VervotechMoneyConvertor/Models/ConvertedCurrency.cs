using System;

namespace VervotechMoneyConvertor.Models
{
    public class ConvertedCurrency
    {
        public decimal Amount { get; set; }

        public string FromCurrency { get; set; }

        public string ToCurrency { get; set; }

        public decimal ConvertedAmount { get; set; }

    }
}
