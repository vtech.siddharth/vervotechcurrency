using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VervotechMoneyConvertor.Models
{
    public class Data
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("_data")]
        public decimal Amount { get; set; }

        public string FromCurrency { get; set; }

        public string ToCurrency { get; set; }

        public decimal ConvertedAmount { get; set; }
    }
}
