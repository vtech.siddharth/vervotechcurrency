using System;

namespace VervotechMoneyConvertor.Models
{
    public class Currency
    {
         public decimal Amount { get; set; }

        public string FromCurrency { get; set; }

        public string ToCurrency { get; set; }
        public Currency(decimal Amount, string FromCurrency, string ToCurrency)
        {
            this.Amount = Amount;
            this.FromCurrency = FromCurrency;
            this.ToCurrency = ToCurrency;
        }
    }
}
