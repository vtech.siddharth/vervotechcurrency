using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VervotechMoneyConvertor.Logger;
using VervotechMoneyConvertor.Models;
using VervotechMoneyConvertor.Services;

namespace VervotechMoneyConvertor.Controllers
{

    [ApiController]
    [Route("[controller]")]

    public class CurrencyExchangeController : ControllerBase
    {
        private readonly ILogger _ilogger;

        CurrencyService _currencyService;
        public CurrencyExchangeController(CurrencyService currencyService, ILogger ilogger)
        {
            this._currencyService = currencyService;
            this._ilogger = ilogger;
        }

        [HttpPost]
        public ActionResult<List<ConvertedCurrency>> GetAmount(List<Currency> currency)
        {

            var jsonRequest = JsonConvert.SerializeObject(currency);

            List<ConvertedCurrency> currencies = _currencyService.GetConvertedValue(currency).Result;
            var jsonResponse = JsonConvert.SerializeObject(currencies);
            Log _log = new Log();
            _log.request = jsonRequest;
            _log.response = jsonResponse;
           
            _ilogger.LogData(_log);
            return currencies;
        }

        

    }

}
